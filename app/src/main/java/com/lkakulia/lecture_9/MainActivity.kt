package com.lkakulia.lecture_9

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    val userData: ArrayList<Array<String>> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    //add button listeners
    private fun init() {
        addUserBtn.setOnClickListener(this)
        removeUserBtn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        //check for empty fields
        if (firstName.text.isEmpty() || lastName.text.isEmpty() ||
            age.text.isEmpty() || email.text.isEmpty()) {
            toastMessage("Please fill in all the fields")
        }

        else {
            val firstNameText = firstName.text.toString()
            val lastNameText = lastName.text.toString()
            val ageText = age.text.toString()
            val emailText = email.text.toString()

            //addUserBtn logic
            if (v!!.id == R.id.addUserBtn) {
                if (userData.size != 0) {
                    for (user in userData) {
                        if (user[3] == emailText) {
                            toastMessage("The user with the email already exists")
                            return
                        }
                    }
                }

                if (isValidEmail(emailText)) {
                    userData.add(arrayOf(firstNameText, lastNameText, ageText, emailText))
                    toastMessage("The user was added")
                }

                else {
                    toastMessage("The email is invalid")
                }
            }

            //removeUserBtn logic
            else {
                if (userData.size != 0) {
                    for (user in userData) {
                        if (user[0] == firstNameText && user[1] == lastNameText &&
                            user[2] == ageText && user[3] == emailText) {
                            userData.remove(user)
                            toastMessage("The user was removed")
                            return
                        }
                    }
                }

                toastMessage("The user does not exist")
            }
        }


    }

    //toast message function
    private fun toastMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    //email validity function
    private fun isValidEmail(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}
